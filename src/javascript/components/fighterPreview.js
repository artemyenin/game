import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  function createProperty(keyValue) {
    const nameElement = createElement({ tagName: 'span', className: 'fighter-preview___property' });
    nameElement.innerText = keyValue.join(': ').replace(/(\w+):/, subStr => subStr.toUpperCase());
    return nameElement;
  }

   if(fighter) {
    const keyValueArrayOfObject = Object.entries(fighter);
   
    fighterElement.append(createFighterImage(fighter));
    keyValueArrayOfObject
    .filter(keyValueAll => keyValueAll[0] !== '_id' && keyValueAll[0] !== 'source')
    .forEach(keyValue => fighterElement.append(createProperty(keyValue)));
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  console.log(fighter);
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  imgElement.style.width = '50%';

  return imgElement;
}
